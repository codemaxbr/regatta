<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<title>[Regatta]</title>

		<link href="css/bootstrap.min.css" rel="stylesheet">
		<link href="css/font-awesome.min.css" rel="stylesheet">
		<link href="css/style.css" rel="stylesheet">
		<link href="css/ion.rangeSlider.css" rel="stylesheet">
		<link href="css/ion.rangeSlider.skinNice.css" rel="stylesheet">

		<link href='https://fonts.googleapis.com/css?family=Roboto:400,500,700,900,300' rel='stylesheet' type='text/css'>
    	<link href='https://fonts.googleapis.com/css?family=Lato:400,700,900' rel='stylesheet' type='text/css'>
    	<link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>

		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	<body>
		<!-- Início Header -->
		<header>
			<nav class="navbar navbar-default">
				<div class="container">

					<div class="navbar-header">
						<a href="#" class="navbar-brand">
							<img src="images/logo.jpg" alt="Regatta Tecidos" />
						</a>

						<button type="button" class="collapsed navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-6" aria-expanded="false">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
					</div>

					<div class="top_bar">
						<a class="btn btn-link" href="#">Minha Conta</a>
						<a class="btn btn-link" href="#">Login</a>
						<a class="btn btn-link" href="#">Cadastre-se</a>
					</div>

					<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-6">
						<ul class="nav navbar-nav">
							<li><a href="#">Home</a></li>
							<li><a href="#">Tecidos</a></li>
							<li><a href="#">Casa</a></li>
							<li><a href="#">Papel de Parede</a></li>
							<li><a href="#">Acessórios</a></li>
						</ul>

						<ul class="nav navbar-nav navbar-right">
							<li>
					        	<a href="#" class="btn btn-primary">
					        		<i class="fa fa-search" aria-hidden="true"></i>
					        	</a>
					        </li>
					        <li>
					        	<a href="#" class="btn btn-primary">
					        		<i class="fa fa-shopping-cart" aria-hidden="true"></i>
					        		(2 itens)
					        	</a>
					        </li>
					    </ul>
					</div> 
				</div> 
			</nav>
		</header>
		<!-- Fim do Header -->

		<div class="container">
			<div class="col-md-3 menu_nav">
				<h1>Categorias</h1>

				<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
				  	<div class="panel panel-default">
				    	<div class="panel-heading" role="tab" id="headingOne">
				      		<h4 class="panel-title">
				        		<a role="button" class="fa fa-circle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
				          			Utilização
				        		</a>
				      		</h4>
				    	</div>
				    	<div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
				      		<div class="panel-body">
				        		<ul>
				        			<li class="fa fa-circle"><a href="#">Camurça</a></li>
				        			<li class="fa fa-circle"><a href="#">Camurça</a></li>
				        			<li class="fa fa-circle"><a href="#">Camurça</a></li>
				        			<li class="fa fa-circle"><a href="#">Camurça</a></li>
				        			<li class="fa fa-circle"><a href="#">Camurça</a></li>
				        			<li class="fa fa-circle"><a href="#">Camurça</a></li>
				        		</ul>
				      		</div>
				    	</div>
				  	</div><!-- End Collapse 1 -->

				  	<div class="panel panel-default">
				    	<div class="panel-heading" role="tab" id="headingTwo">
				      		<h4 class="panel-title">
				        		<a role="button" class="fa fa-circle" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
				          			Padrões
				        		</a>
				      		</h4>
				    	</div>
				    	<div id="collapseTwo" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingTwo">
				      		<div class="panel-body">
				        		<ul>
				        			<li class="fa fa-circle"><a href="#">Camurça</a></li>
				        			<li class="fa fa-circle"><a href="#">Camurça</a></li>
				        			<li class="fa fa-circle"><a href="#">Camurça</a></li>
				        			<li class="fa fa-circle"><a href="#">Camurça</a></li>
				        			<li class="fa fa-circle"><a href="#">Camurça</a></li>
				        			<li class="fa fa-circle"><a href="#">Camurça</a></li>
				        		</ul>
				      		</div>
				    	</div>
				  	</div><!-- End Collapse 1 -->
				</div><!-- End Accordion -->

				<div class="row">
					<div class="col-md-12">
						<h1>Seleção de Filtro</h1>
						<label>Preço por</label>
						<input type="text" id="filtro_preco" name="example_name" value="" />
					</div>
				</div>

				<div class="row">
					<div class="col-md-12">
						<label>Color</label>
						<a class="btn">
							<i class="fa fa-square marrom" aria-hidden="true"></i>
						</a>

						<a class="btn active">
							<i class="fa fa-square bege" aria-hidden="true"></i>
						</a>

						<a class="btn">
							<i class="fa fa-square laranja" aria-hidden="true"></i>
						</a>

						<a class="btn">
							<i class="fa fa-square amarelo" aria-hidden="true"></i>
						</a>
					</div>
				</div>
			</div>

			<div class="col-md-9">
				<!-- Produtos -->
				<div class="produtos">
					<div class="col-md-12 titulo">
						<h2>Foram encontrados 10 produtos nesta categoria (em 0,011 segundos).</h2>
					</div>

					<div class="col-md-3 col-xs-6 text-center produto">
						<div class="foto_produto">
							<img src="images/tecidos/tecido_2.jpg" class="img-responsive">
						</div>
						<a href="#" class="link_produto">Sumbrella Labirinto - Wheat/g</a>
						<div class="preco">
							<p class="preco_por">R$ 67,05</p>
							<p class="preco_de">R$ 102,50</p>
						</div>

						<div class="botoes">
							<a class="btn addCarrinho" href="#">Add Carrinho</a>
							<a class="btn addFavorito" href="#"><i class="fa fa-heart-o" aria-hidden="true"></i></a>
						</div>
					</div>

					<div class="col-md-3 col-xs-6 text-center produto">
						<div class="foto_produto">
							<img src="images/tecidos/tecido_3.jpg" class="img-responsive">
						</div>
						<a href="#" class="link_produto">Sunbrella Folha - Antibeige/B</a>
						<div class="preco">
							<p class="preco_por">R$ 67,05</p>
						</div>

						<div class="botoes">
							<a class="btn addCarrinho" href="#">Add Carrinho</a>
							<a class="btn addFavorito" href="#"><i class="fa fa-heart-o" aria-hidden="true"></i></a>
						</div>
					</div>

					<div class="col-md-3 col-xs-6 text-center produto">
						<div class="foto_produto">
							<img src="images/tecidos/tecido_4.jpg" class="img-responsive">
						</div>
						<a href="#" class="link_produto">Sunbrella Arabesco - Antibeig</a>
						<div class="preco">
							<p class="preco_por">R$ 67,05</p>
						</div>

						<div class="botoes">
							<a class="btn addCarrinho" href="#">Add Carrinho</a>
							<a class="btn addFavorito" href="#"><i class="fa fa-heart-o" aria-hidden="true"></i></a>
						</div>
					</div>

					<div class="col-md-3 col-xs-6 text-center produto">
						<div class="foto_produto">
							<img src="images/tecidos/tecido_6.jpg" class="img-responsive">
						</div>
						<a href="#" class="link_produto">Sunbrella Listra - Wheat/Gree</a>
						<div class="preco">
							<p class="preco_por">R$ 67,05</p>
						</div>

						<div class="botoes">
							<a class="btn addCarrinho" href="#">Add Carrinho</a>
							<a class="btn addFavorito" href="#"><i class="fa fa-heart-o" aria-hidden="true"></i></a>
						</div>
					</div>

					<div class="col-md-3 col-xs-6 text-center produto">
						<div class="foto_produto">
							<img src="images/tecidos/tecido_7.jpg" class="img-responsive">
						</div>
						<a href="#" class="link_produto">Sunbrella Listra - Antibeige</a>
						<div class="preco">
							<p class="preco_por">R$ 67,05</p>
						</div>

						<div class="botoes">
							<a class="btn addCarrinho" href="#">Add Carrinho</a>
							<a class="btn addFavorito" href="#"><i class="fa fa-heart-o" aria-hidden="true"></i></a>
						</div>
					</div>

					<div class="col-md-3 col-xs-6 text-center produto">
						<div class="foto_produto">
							<img src="images/tecidos/tecido_1.gif" class="img-responsive">
						</div>
						<a href="#" class="link_produto">Sunb.Toldo 152CM - Captain Na</a>
						<div class="preco">
							<p class="preco_por">R$ 67,05</p>
						</div>

						<div class="botoes">
							<a class="btn addCarrinho" href="#">Add Carrinho</a>
							<a class="btn addFavorito" href="#"><i class="fa fa-heart-o" aria-hidden="true"></i></a>
						</div>
					</div>

					<div class="col-md-12 pagination">
						<div class="arrows">
							<a class="btn active" href="#">1</a>
							<a class="btn" href="#">2</a>
							<a class="btn" href="#">3</a>
							<a class="btn" href="#">...</a>
							<a class="btn" href="#">5</a>
							<a class="btn" href="#"><i class="fa fa-angle-right"></i></a>
						</div>
					</div>
				</div>
				<!-- ./ Produtos -->
			</div>
		</div>

		<footer>
			<div class="container">
				<div class="col-md-3">
					<h1>Newsletter</h1>

					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>

					<div class="input-group">
				      	<input type="text" class="form-control" placeholder="E-mail...">
				      	<span class="input-group-btn">
				        	<button class="btn btn-primary" type="button">
				        		<i class="fa fa-plus" aria-hidden="true"></i>
				        	</button>
				      	</span>
				    </div><!-- /input-group -->
				</div>

				<div class="col-md-3">
					<h1>Menu</h1>
					<ul>
						<li class="fa fa-circle"><a href="#">Tecido</a></li>
						<li class="fa fa-circle"><a href="#">Casa</a></li>
						<li class="fa fa-circle"><a href="#">Papel de Parede</a></li>
						<li class="fa fa-circle"><a href="#">Acessórios</a></li>
						<li class="fa fa-circle"><a href="#">Tecido</a></li>
						<li class="fa fa-circle"><a href="#">Showroom</a></li>
						<li class="fa fa-circle"><a href="#">Institucional</a></li>
					</ul>
				</div>

				<div class="col-md-3">
					<h1>O Nosso Apoio</h1>
					<ul>
						<li class="fa fa-circle"><a href="#">Informações de Entrega</a></li>
						<li class="fa fa-circle"><a href="#">Política de privacidade</a></li>
						<li class="fa fa-circle"><a href="#">Termos & Condições</a></li>
						<li class="fa fa-circle"><a href="#">Contato</a></li>
					</ul>
				</div>

				<div class="col-md-3">
					<h1>Nossos Serviços</h1>
					<ul>
						<li class="fa fa-circle"><a href="#">Minha Conta</a></li>
						<li class="fa fa-circle"><a href="#">Histórico de Pedidos</a></li>
						<li class="fa fa-circle"><a href="#">Retorna</a></li>
						<li class="fa fa-circle"><a href="#">Mapa do Site</a></li>
					</ul>
				</div>
			</div>
		</footer>
		<div class="footer">
			<div class="container">
				<p>
					<b>Endereço: </b> Av. Queiroz Filho, 944 - Vila Leopoldina - Fone/Fax: (11) 3834-2724
				</p>
				<p>
					<b>CNPJ: </b> 38.024.501/0001-80
				</p>
			</div>
		</div>

		<script src="js/jquery.min.js"></script>
		<script src="js/bootstrap.min.js"></script>
		<script src="js/ion-rangeSlider/ion.rangeSlider.min.js"></script>
		<script type="text/javascript">
			$(function(){
				$('#collapseOne').collapse({
				  	toggle: false
				});

				$('#collapseOne').collapse('hide');

				$("#filtro_preco").ionRangeSlider({
				    type: "double",
				    min: 0,
				    max: 5000,
				    from: 40,
				    prefix: "R$",
				    postfix: ",00"
				});
			})
		</script>
	</body>
</html>
